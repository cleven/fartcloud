using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Xamarin.Forms;
using FartCloud.Mobile.Interfaces;
using Uri = Android.Net.Uri;
using FartCloud.Mobile.Droid.Helpers;
using FartCloud.Models.Interfaces;

namespace FartCloud.Mobile.Droid {
    [Activity(Label = "Ramen Wage",
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]

    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity, IAppLinkService {
        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            global::Xamarin.Forms.Forms.Init(this, bundle);
            global::Xamarin.Forms.Forms.SetTitleBarVisibility(Xamarin.Forms.AndroidTitleBarVisibility.Never);
            LoadApplication(new App());

            CredentialsService credService = new CredentialsService();
            CredentialHelper.Credentials = credService;
        }

        public void OpenStoreLink(string storeLinkPath, string httpStoreLinkPath, string appleId) {
            try {
                string url = string.Format(storeLinkPath, this.Application.PackageName);
                Intent intent = new Intent(Intent.ActionView);
                intent.SetData(Uri.Parse(url));
                StartActivity(intent);
            } catch (Exception) {
                string url = string.Format(httpStoreLinkPath, this.Application.PackageName);
                Intent intent = new Intent(Intent.ActionView);
                intent.SetData(Uri.Parse(url));
                StartActivity(intent);
            }
        }

        
    }
}
