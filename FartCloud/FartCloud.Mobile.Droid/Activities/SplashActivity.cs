using Android.App;
using Android.Content;
using Android.OS;
using System.Threading;

namespace FartCloud.Mobile.Droid.Activities {

    [Activity(Label = "Ramen Wage",
         Icon = "@drawable/icon",
         MainLauncher = true,
         NoHistory = true,
         Theme = "@style/Theme.Splash")]
    public class SplashActivity : Activity {

        protected override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            Thread.Sleep(3000);
            var intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
            Finish();
        }
    }
}
