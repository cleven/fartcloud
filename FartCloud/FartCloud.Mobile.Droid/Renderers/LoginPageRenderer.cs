using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using FartCloud.Mobile.Pages;
using Xamarin.Forms;
using FartCloud.Mobile.Droid.Renderers;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(LoginPage), typeof(LoginPageRenderer))]


namespace FartCloud.Mobile.Droid.Renderers {
    public class LoginPageRenderer  : PageRenderer {

        protected override void OnElementChanged(ElementChangedEventArgs<Page> e) {
            base.OnElementChanged(e);

            if(e.OldElement == null) // perform initial setup
            {
                var page = e.NewElement as LoginPage;
                page.BackgroundImage = "background.png";
                
            }
        }

    }
}
