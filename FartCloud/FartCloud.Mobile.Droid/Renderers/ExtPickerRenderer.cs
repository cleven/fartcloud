using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using FartCloud.Mobile.Controls;
using FartCloud.Mobile.Droid.Renderers;

[assembly: ExportRenderer(typeof(ExtPicker), typeof(ExtPickerRenderer))]


namespace FartCloud.Mobile.Droid.Renderers {
    public class ExtPickerRenderer : PickerRenderer {

        Android.Graphics.Color textColor = Android.Graphics.Color.Black;
        Android.Graphics.Color hintColor = Android.Graphics.Color.Gray;

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e) {
            base.OnElementChanged(e);

            var nativeControl = (global::Android.Widget.EditText)Control;
            ExtPicker picker = (ExtPicker)e.NewElement;

            if (picker.TextColor != null) {
                textColor = picker.TextColor.ToAndroid();
            }

            if (picker.HintColor != null) {
                hintColor = picker.HintColor.ToAndroid();
            }

            nativeControl.SetHintTextColor(hintColor);
            nativeControl.SetTextColor(textColor);
        }
    }
}
