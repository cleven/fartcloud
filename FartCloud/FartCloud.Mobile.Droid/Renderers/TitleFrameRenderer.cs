using Xamarin.Forms;
using FartCloud.Mobile.Droid.Renderers;
using Xamarin.Forms.Platform.Android;
using FartCloud.Mobile.Controls;

[assembly: ExportRenderer(typeof(TitleLayout), typeof(TitleFrameRenderer))]


namespace FartCloud.Mobile.Droid.Renderers {
    public class TitleFrameRenderer : VisualElementRenderer<StackLayout> {

        protected override void OnElementChanged(ElementChangedEventArgs<StackLayout> e) {
            base.OnElementChanged(e);
            SetBackgroundResource(Resource.Drawable.blue_rect);
        }
    }
}
