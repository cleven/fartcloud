using System;
using System.Linq;
using Xamarin.Forms;
using FartCloud.Mobile.Droid.Helpers;
using Xamarin.Auth;
using FartCloud.Models.Interfaces;

[assembly: Dependency(typeof(CredentialsService))]

namespace FartCloud.Mobile.Droid.Helpers {


    public class CredentialsService : ICredentialsService {
        

        public string Token {
            get {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService(App.AppName).FirstOrDefault();
                return (account != null) ? account.Properties["Token"] : null;
            }
        }

        public Guid RefreshToken {
            get {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService(App.AppName).FirstOrDefault();
                if (account == null)
                    return Guid.Empty;
                if (string.IsNullOrWhiteSpace(account.Properties["RefreshToken"]))
                    return Guid.Empty;

                string tenantIdentifier = account.Properties["RefreshToken"];
                Guid tenantGuid = Guid.Empty;
                Guid.TryParse(tenantIdentifier, out tenantGuid);
                return tenantGuid;
            }
        }

        public void SaveCredentials(string Username, string Token, string RefreshToken) {
            Account account = new Account {
                Username = Username
            };
            account.Properties.Add("Token", Token);
            account.Properties.Add("RefreshToken", RefreshToken);
            AccountStore.Create(Forms.Context).Save(account, App.AppName);
        }

        public void DeleteCredentials() {
            var account = AccountStore.Create(Forms.Context).FindAccountsForService(App.AppName).FirstOrDefault();
            if (account != null) {
                AccountStore.Create(Forms.Context).Delete(account, App.AppName);
            }
        }


        public bool DoCredentialsExist() {
            var accounts = AccountStore.Create(Forms.Context).FindAccountsForService(App.AppName);
            return accounts.Any() ? true : false;
        }
    }
}
