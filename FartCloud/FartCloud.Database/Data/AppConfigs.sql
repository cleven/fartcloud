﻿PRINT 'Importing [dbo].[AppConfigs]'

CREATE TABLE #TempImportAppConfigs (
    [AppConfigId]    INT             NOT NULL   PRIMARY KEY CLUSTERED,
    [AppConfigName]  NVARCHAR(20)    NOT NULL,
    [AppConfigValue] NVARCHAR(128)   NOT NULL,
	[IsMobile]       BIT             NOT NULL,
	[IsWebAPI]       BIT             NOT NULL
)
GO

INSERT INTO #TempImportAppConfigs ([AppConfigId], [AppConfigName], [AppConfigValue], [IsMobile], [IsWebAPI])
VALUES
(1, 'APISecret', 'co6dooGho7aNgeip0ooC1Oughu0uCieheeY5yur9uuwuChachiasovahf7iegohshush9Chahc0KaiChae7tai2Aeyu7mua4Aweimahzu7Ahmiegh3iotaithiem7Ao0', 0, 1),
(2, 'APITokenTimeout', '24', 0, 1)

DELETE FROM [dbo].[AppConfig] WHERE [AppConfigId] NOT IN (
	SELECT [AppConfigId] FROM #TempImportAppConfigs
)


SET IDENTITY_INSERT [dbo].[AppConfig] ON
MERGE INTO [dbo].[AppConfig] AS destination
	USING (SELECT * FROM #TempImportAppConfigs) AS source
	ON (destination.[AppConfigId] = source.[AppConfigId])
	WHEN MATCHED THEN
		UPDATE SET
			[AppConfigName] = source.[AppConfigName],
			[AppConfigValue] = source.[AppConfigValue],
			[IsMobile] = source.[IsMobile],
			[IsWebAPI] = source.[IsWebAPI]
	WHEN NOT MATCHED THEN
		INSERT ([AppConfigId], [AppConfigName], [AppConfigValue], [IsMobile], [IsWebAPI])
		VALUES (source.[AppConfigId], source.[AppConfigName], source.[AppConfigValue], source.[IsMobile], source.[IsWebAPI]);
SET IDENTITY_INSERT [dbo].[AppConfig] OFF

DROP TABLE #TempImportAppConfigs
GO