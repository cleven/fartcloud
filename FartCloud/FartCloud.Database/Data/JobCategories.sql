﻿PRINT 'Importing [dbo].[JobCategories]'

CREATE TABLE #TempImportJobCategories (
    [JobCategoryId]    INT             NOT NULL   PRIMARY KEY CLUSTERED,
    [JobCategoryName]  NVARCHAR(50)    NOT NULL,
	[Active]           BIT             NOT NULL,
)
GO

INSERT INTO #TempImportJobCategories ([JobCategoryId], [JobCategoryName], [Active])
VALUES
(1, 'Lawn Care', 1),
(2, 'Furniture', 1),
(3, 'Pet Sitting', 1),
(4, 'Cleaning', 1),
(5, 'Painting', 1),
(6, 'Gardening', 1),
(7, 'Outdoor Cleaning and Maintenance', 1),
(8, 'Decoration Removal / Installation', 1),
(9, 'Babysitting', 1),
(10, 'House Sitting', 1),
(11, 'Tutoring', 1),
(12, 'Grocery Shopping', 1)

DELETE FROM [dbo].[JobCategory] WHERE [JobCategoryId] NOT IN (
	SELECT [JobCategoryId] FROM #TempImportJobCategories
)


SET IDENTITY_INSERT [dbo].[JobCategory] ON

MERGE INTO [dbo].[JobCategory] AS destination
USING (SELECT * FROM #TempImportJobCategories) AS source
	
	ON (destination.[JobCategoryId] = source.[JobCategoryId])
	WHEN MATCHED THEN
		UPDATE SET
			[JobCategoryName] = source.[JobCategoryName],
			[Active] = source.[Active]
	WHEN NOT MATCHED THEN
		INSERT ([JobCategoryId], [JobCategoryName], [Active])
		VALUES (source.[JobCategoryId], source.[JobCategoryName], source.[Active]);
SET IDENTITY_INSERT [dbo].[JobCategory] OFF

DROP TABLE #TempImportJobCategories
GO