﻿CREATE TABLE [dbo].[JobCategory]
(
	[JobCategoryId]   INT         NOT NULL PRIMARY KEY CLUSTERED IDENTITY(1,1),
	[JobCategoryName] VARCHAR(50) NOT NULL,
	[Active]          BIT         NOT NULL,
)
