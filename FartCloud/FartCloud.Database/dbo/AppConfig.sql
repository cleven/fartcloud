﻿CREATE TABLE [dbo].[AppConfig]
(
	[AppConfigId]    INT          NOT NULL PRIMARY KEY CLUSTERED IDENTITY(1,1),
	[AppConfigName]  VARCHAR(50)  NOT NULL,
	[AppConfigValue] VARCHAR(128) NOT NULL,
	[IsMobile]     BIT          NOT NULL,
	[IsWebAPI]     BIT          NOT NULL
)
