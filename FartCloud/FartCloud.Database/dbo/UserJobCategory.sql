﻿CREATE TABLE [dbo].[UserJobCategory]
(
	[UserJobCategoryId] INT NOT NULL IDENTITY(1,1) PRIMARY KEY CLUSTERED,
	[JobCategoryId]     INT NOT NULL,
	[UserId]            INT NOT NULL

	CONSTRAINT FK_UserJobCategory_JobCategoryId FOREIGN KEY ([JobCategoryId]) REFERENCES [dbo].[JobCategory] ([JobCategoryId]),
	CONSTRAINT FK_UserJobCategory_UserId FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId]),

)
