﻿CREATE TABLE [dbo].[User]
(
	[UserId]       INT              NOT NULL PRIMARY KEY CLUSTERED IDENTITY(1,1),
	[FirstName]    VARCHAR(25)      NOT NULL,
	[LastName]     VARCHAR(25)      NOT NULL,
	[Phone]        VARCHAR(12)      NOT NULL,
	[EmailAddress] VARCHAR(100)     NOT NULL,
	[PasswordHash] VARCHAR(100)     NOT NULL,
	[RefreshToken] UNIQUEIDENTIFIER NOT NULL,
	[DateCreated]  DATETIME         NOT NULL,
	[DateModified] DATETIME			    NULL,
)
