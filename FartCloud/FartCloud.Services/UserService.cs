using FartCloud.Models.Data;
using FartCloud.Models.Business;
using System;
using System.Linq;
using System.Collections.Generic;

namespace FartCloud.Services
{
    public class UserService
    {
        public LoginResultModel Login(LoginModel model) {
            using (var db = new StorageEntities()) {
                var user = db.Users
                    .Where(x => x.EmailAddress == model.Username)
                    .FirstOrDefault();

                if (user == null)
                    throw new Exception("Username / Password is incorrect");

                bool isVerified = SecurityService.VerifyHashedPassword(user.PasswordHash, model.Password);
                if (isVerified == false)
                    throw new Exception("Username / Password is incorrect");

                
                LoginResultModel result = new LoginResultModel() {
                    Token = SecurityService.GenerateToken(),
                    RefreshToken = user.RefreshToken
                };

                return result;
            }
        }

        
        public LoginResultModel Register(Register model) {
            using (var db = new StorageEntities()) {
                model.User.DateCreated = DateTime.Now;
                model.User.DateModified = null;
                model.User.RefreshToken = Guid.NewGuid();
                model.User.PasswordHash = SecurityService.GenerateHashedPassword(model.User.PasswordHash);

                db.Users.Add(model.User);
                db.SaveChanges();

                foreach (int item in model.JobCategoryIds) {
                    UserJobCategory dm = new UserJobCategory() {
                        UserId = model.User.UserId,
                        JobCategoryId = item
                    };
                    db.UserJobCategorys.Add(dm);
                }
                db.SaveChanges();

                LoginResultModel result = new LoginResultModel() {
                    RefreshToken = model.User.RefreshToken,
                    Token = SecurityService.GenerateToken(),
                };

                return result;
            }
        }

        
        
    }
}
