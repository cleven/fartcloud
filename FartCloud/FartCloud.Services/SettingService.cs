using FartCloud.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FartCloud.Services {
    public class SettingService {

        public static List<AppConfig> Settings { get; set; }


        public List<AppConfig> GetSettings(bool IsMobile, bool IsWebAPI) {
            using (var db = new StorageEntities()) {
                return db.AppConfigs
                    .Where(x=> x.IsWebAPI == IsWebAPI && x.IsMobile == IsMobile)
                    .ToList();
            }
        }

    }
}
