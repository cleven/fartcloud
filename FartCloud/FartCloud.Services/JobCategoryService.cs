using FartCloud.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FartCloud.Services {
    public class JobCategoryService {

        public List<JobCategory> GetJobCategories() {
            using (var db = new StorageEntities()) {
                var result = db.JobCategories
                    .Where(x => x.Active)
                    .OrderBy(x=> x.JobCategoryName)
                    .ToList();
                return result;
            }
        }

    }
}
