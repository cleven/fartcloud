using FartCloud.Models.Data;
using System.Data.Entity;

namespace FartCloud.Services {
    public class StorageEntities : DbContext {

        public StorageEntities() : base ("name=StorageEntities") {
            this.Database.CommandTimeout = 600;
            Database.SetInitializer<StorageEntities>(null);
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            
        }

        public DbSet<AppConfig> AppConfigs { get; set; }
        public DbSet<JobCategory> JobCategories { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserJobCategory> UserJobCategorys { get; set; }
        

    }
}
