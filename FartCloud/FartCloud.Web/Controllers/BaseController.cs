using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FartCloud.WebControl;
using System.Configuration;
using Microsoft.Owin.Security;
using FartCloud.Models;
using System.Security.Claims;

namespace FartCloud.Controllers
{
    public class BaseController : Controller
    {
        public LoggerClient loggerService = new LoggerClient();

        public IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        //public <UserModel> CurrentUser {
        //    get {
        //        ///Needs Implementation
        //    }
        //}

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            WebClientConstants.Url = ConfigurationManager.AppSettings["ApiUrl"];

            string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            ViewBag.BaseUrl = baseUrl;

            base.OnActionExecuting(filterContext);
        }

    }
}
