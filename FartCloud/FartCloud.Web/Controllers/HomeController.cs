using System;
using System.Web.Mvc;
using System.Threading.Tasks;
using FartCloud.Models;
using Newtonsoft.Json;

namespace FartCloud.Controllers
{
    public class HomeController : BaseController
    {

        public ActionResult Index()
        {
            return View();
        }
    }
}
