using FartCloud.Models.Business;
using FartCloud.Models.Data;
using FartCloud.Models.Interfaces;
using FartCloud.WebControl;
using System.Collections.Generic;

namespace FartCloud.Client {
    public class SettingClient {

        public static async void GetAppSettings(IApp app) {
            LoggerClient.Log("Retrieving all application settings from server.");

            var result = await new WebClient().GetAsync<List<AppConfig>>(WebClientConstants.GetApplicationConfiguration);
            if (result.Errored)
                LoggerClient.Log(result.Exception);

            var jobCategoryResult = await JobCategoryClient.GetJobCategories();
            if (jobCategoryResult.Errored)
                LoggerClient.Log(jobCategoryResult.Exception);


            Settings settings = new Settings();
            settings.AppConfigs = result.Payload;
            settings.JobCategories = jobCategoryResult.Payload;
            

            app.SetMainPage(settings);
            LoggerClient.Log("Finished retrieving application settings from server.");
        }
    }
}
