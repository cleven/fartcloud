using FartCloud.Models.Business;
using FartCloud.Models.Data;
using FartCloud.WebControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FartCloud.Client {
    public class JobCategoryClient {

        public static async Task<JsonPayload<List<JobCategory>>> GetJobCategories() {
            string path = string.Format(WebClientConstants.GetJobCategories);
            using (var client = new BaseHttpClient()) {
                var response = await client.GetAsync(WebClientConstants.Url + path);
                string content = await response.Content.ReadAsStringAsync();
                var result = WebClient.Convert<List<JobCategory>>(content);

                return result;
            }
        }

    }
}
