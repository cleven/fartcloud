namespace FartCloud.WebControl
{
    public class WebClientConstants
    {
        public static string Url { get; set; }

        /// <summary>
        /// User Endpoints
        /// </summary>
        public const string Login = "/api/User/Login";
        public const string Register = "/api/User/Register";

        public const string GetApplicationConfiguration = "/api/App/Settings";
        public const string GetJobCategories = "/api/JobCategory/List";
    }
}
