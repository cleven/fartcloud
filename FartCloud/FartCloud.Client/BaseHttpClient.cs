using FartCloud.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FartCloud.Client {
    public class BaseHttpClient : HttpClient {

        public BaseHttpClient() {
            DefaultRequestHeaders.Add("access-token", "Bearer ." + CredentialHelper.Credentials.Token);
            DefaultRequestHeaders.Add("refresh-token", CredentialHelper.Credentials.RefreshToken.ToString());
        }

    }
}
