using Newtonsoft.Json;
using FartCloud.Models.Business;
using FartCloud.Models.Data;
using FartCloud.WebControl;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace FartCloud.Client {
    public class UserClient {

        public static async Task<JsonPayload<LoginResultModel>> Login(LoginModel model) {
            string path = string.Format(WebClientConstants.Login);
            using (var client = new BaseHttpClient()) {
                var prms = new List<KeyValuePair<string, string>> {
                    new KeyValuePair<string, string>("Username", model.Username),
                    new KeyValuePair<string, string>("Password", model.Password),
                };
                
                var response = await client.PostAsync(WebClientConstants.Url + path, new FormUrlEncodedContent(prms));
                string content = await response.Content.ReadAsStringAsync();
                var result = WebClient.Convert<LoginResultModel>(content);
                
                return result;
            }
        }


        public static async Task<JsonPayload<LoginResultModel>> Register(User model, List<int> JobCategoryIds) {
            string path = string.Format(WebClientConstants.Register);
            using (var client = new BaseHttpClient()) {
                var prms = new List<KeyValuePair<string, string>> {
                    new KeyValuePair<string, string>("User.EmailAddress", model.EmailAddress),
                    new KeyValuePair<string, string>("User.FirstName", model.FirstName),
                    new KeyValuePair<string, string>("User.LastName", model.LastName),
                    new KeyValuePair<string, string>("User.PasswordHash", model.PasswordHash),
                    new KeyValuePair<string, string>("User.Phone", model.Phone),
                };
                for (int i = 0; i < JobCategoryIds.Count; i++) {
                    prms.Add(new KeyValuePair<string, string>("JobCategoryIds[" + i + "]", JobCategoryIds[i].ToString()));
                }

                var response = await client.PostAsync(WebClientConstants.Url + path, new FormUrlEncodedContent(prms));
                string content = await response.Content.ReadAsStringAsync();
                var result = WebClient.Convert<LoginResultModel>(content);
                return result;
            }
        }
    }
}
