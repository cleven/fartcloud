using FartCloud.Models.Business;
using FartCloud.Services;
using System;
using System.Data.Entity.Validation;
using System.Web.Http;

namespace FartCloud.Api.Controllers
{
    public class BaseApiController : ApiController
    {
        public UserService userService = new UserService();
        public JobCategoryService jobCategoryService = new JobCategoryService();

        public BaseApiController() {
            if (SettingService.Settings == null)
                SettingService.Settings = new SettingService().GetSettings(false, true);
        }

        /// <summary> 
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="partial"></param>
        /// <param name="type"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        protected JsonPayload<T> Result<T>(Func<T> data, string partial = null) {
            JsonPayload<T> result = new JsonPayload<T>();
            try {
                var invokedResult = data();
                result.Payload = invokedResult;
            } catch (DbEntityValidationException ex) {
                result.Errored = true;
                result.Exception = GetEntityValidationErrors(ex);
            } catch (Exception ex) {
                result.Errored = true;
                result.Exception = ex.Message;
            }
            return result;
        }

        protected string GetEntityValidationErrors(DbEntityValidationException ex) {
            string error = string.Empty;
            foreach (DbEntityValidationResult item in ex.EntityValidationErrors) {
                foreach (DbValidationError tmp in item.ValidationErrors) {
                    error += tmp.ErrorMessage + Environment.NewLine;
                }
            }
            return error;
        }

        

    }
}
