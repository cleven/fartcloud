using FartCloud.Models.Data;
using FartCloud.Models.Business;
using System.Web.Http;
using System.Collections.Generic;

namespace FartCloud.Api.Controllers {

    [RoutePrefix("api/User")]
    public class UserController : BaseApiController {

        [HttpPost]
        public JsonPayload<LoginResultModel> Login(LoginModel model) {
            return Result(() => userService.Login(model));
        }

        [HttpPost]
        public JsonPayload<LoginResultModel> Register(Register model) {
            return Result(() => userService.Register(model));
        }


    }
}
