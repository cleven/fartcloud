using FartCloud.Models.Data;
using FartCloud.Models.Business;
using System.Collections.Generic;
using System.Web.Http;

namespace FartCloud.Api.Controllers {

    [RoutePrefix("api/JobCategory")]
    public class JobCategoryController : BaseApiController {

        [HttpGet]
        public JsonPayload<List<JobCategory>> List() {
            return Result(() => jobCategoryService.GetJobCategories());
        }
    }
}
