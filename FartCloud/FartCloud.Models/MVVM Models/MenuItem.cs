using System;

namespace FartCloud.Models {
    public class MenuModel {
        public string Title { get; set; }

        public string IconSource { get; set; }

        public Type TargetType { get; set; }
    }
}

