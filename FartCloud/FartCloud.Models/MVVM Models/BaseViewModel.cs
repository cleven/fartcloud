using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FartCloud.Models.MVVM_Models {
    public class BaseViewModel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;

        internal virtual Task Initialize(params object[] args) {
            return Task.FromResult(0);
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "") {
            if (PropertyChanged != null)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = "") {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        protected void SetField<T>(ref T field, T value, List<string> dependentProperties, [CallerMemberName] string propertyName = null) // udate this to use expression trees
        {
            if (!ReferenceEquals(field, value)) {
                field = value;
                OnPropertyChanged(propertyName);

                foreach (var d in dependentProperties) {
                    OnPropertyChanged(d);
                }
            }
        }
    }
}
