using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FartCloud.Models.MVVM_Models {

    public class CheckedListCellViewModel : BaseViewModel {
        int _id;
        public int Id {
            get { return _id; }
            set { SetField(ref _id, value); }
        }

        string _text;
        public string Text {
            get { return _text; }
            set { SetField(ref _text, value); }
        }

        bool _isSelected;
        public bool IsSelected {
            get { return _isSelected; }
            set {
                SetField(ref _isSelected, value, new List<string> { "ImageSource" });
            }
        }

        public string ImageSource {
            get {
                return IsSelected ? "ic_check_black_24dp.png" : null;
            }
            set {; }
        }
    }
}
