using FartCloud.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FartCloud.Models.Business {
    public class Register {
        public User User { get; set; }
        public List<int> JobCategoryIds { get; set; }
    }
}
