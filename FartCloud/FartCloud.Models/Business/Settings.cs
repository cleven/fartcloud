using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FartCloud.Models.Data;

namespace FartCloud.Models.Business {
    public class Settings {
        public List<JobCategory> JobCategories { get; set; }
        public List<AppConfig> AppConfigs { get; set; }
    }
}
