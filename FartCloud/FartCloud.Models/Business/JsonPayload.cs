
namespace FartCloud.Models.Business
{
    public class JsonPayload<T>
    {

        public T Payload { get; set; }

        public string Raw { get; set; }

        private string _Exception;
        public string Exception {
            get {
                return _Exception;
            }
            set {
                Errored = true;
                _Exception = value; 
            }
        }

        public bool Errored { get; set; }
        
        public R ConvertTo<R>()
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<R>(Raw);
        }
    }
}
