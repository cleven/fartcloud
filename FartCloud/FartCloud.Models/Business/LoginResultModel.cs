using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FartCloud.Models.Business {
    public class LoginResultModel {

        public Guid RefreshToken { get; set; }
        public string Token { get; set; }

    }
}
