using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FartCloud.Models.Data {

    [Table("UserJobCategory")]
    public class UserJobCategory {
        public int UserJobCategoryId { get; set; }
        public int JobCategoryId { get; set; }
        public int UserId { get; set; }
    }
}
