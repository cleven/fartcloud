using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FartCloud.Models.Data {

    [Table("User")]
    public class User {

        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
        public string PasswordHash { get; set; }
        public Guid RefreshToken { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }

    }
}
