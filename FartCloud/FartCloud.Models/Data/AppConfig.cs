using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FartCloud.Models.Data {

    [Table("AppConfig")]
    public class AppConfig {

        public int AppConfigId { get; set; }
        public string AppConfigName { get; set; }
        public string AppConfigValue { get; set; }
        public bool IsMobile { get; set; }
        public bool IsWebAPI { get; set; }
    }
}
