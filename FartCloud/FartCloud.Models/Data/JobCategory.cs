using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FartCloud.Models.Data {

    [Table("JobCategory")]
    public class JobCategory {

        public int JobCategoryId { get; set; }
        public string JobCategoryName { get; set; }
        public bool Active { get; set; }
    }
}
