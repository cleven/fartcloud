using FartCloud.Models.Business;

namespace FartCloud.Models.Interfaces {

    public class AppHelper {
        public static IApp App { get; set; }
    }

    public interface IApp {
        void SetMainPage(Settings settings);
        void Logout();

    }
}
