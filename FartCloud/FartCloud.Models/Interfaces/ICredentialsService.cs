using System;

namespace FartCloud.Models.Interfaces {

    public class CredentialHelper {
        public static ICredentialsService Credentials { get; set; }
    }

    public interface ICredentialsService {
        string Token { get; }
        Guid RefreshToken { get; }

        void SaveCredentials(string Username, string Token, string RefreshToken);

        void DeleteCredentials();

        bool DoCredentialsExist();
    }
}
