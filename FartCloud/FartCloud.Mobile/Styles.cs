using FartCloud.Mobile.Controls;
using System;
using Xamarin.Forms;

namespace FartCloud.Mobile {
    public class Styles {

        public static Style GlobalEntry = new Style(typeof(Entry)) {
            Setters = {
                new Setter { Property = Entry.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand },
                new Setter { Property = Entry.VerticalOptionsProperty, Value = LayoutOptions.Start },
                new Setter { Property = Entry.TextColorProperty, Value = Color.Black },
                new Setter { Property = Entry.PlaceholderColorProperty, Value = Color.Gray },
            },
        };

        public static Style GlobalPicker = new Style(typeof(ExtPicker)) {
            Setters = {
                new Setter { Property = ExtPicker.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand },
                new Setter { Property = ExtPicker.TextColorProperty, Value = Color.Black },
                new Setter { Property = ExtPicker.HintColorProperty, Value = Color.Gray },
            },
        };

        public static Style GlobalContainer = new Style(typeof(ExtStackLayout)) {
            Setters = {
                new Setter { Property = StackLayout.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand },
                new Setter { Property = StackLayout.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand },
                new Setter { Property = StackLayout.MarginProperty, Value = 10 },
            }
        };
        
        public static Style GlobalButton = new Style(typeof(Button)) {
            Setters = {
                new Setter { Property = Button.HeightRequestProperty, Value = 50 },
                new Setter { Property = Button.BackgroundColorProperty, Value = Color.FromHex("478EE5") },
                new Setter { Property = Button.BorderRadiusProperty, Value = 0 },
            }
        };

        public static Style GlobalLabel = new Style(typeof(Label)) {
            Setters = {
                new Setter { Property = Label.TextColorProperty, Value = Color.Black },
                new Setter { Property = Label.FontSizeProperty, Value = 14 },
            }
        };
        
    }
}
