using FartCloud.Mobile.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FartCloud.Mobile.Pages {
    public class FindNewJobPage : BasePage {
        ExtStackLayout container = new ExtStackLayout() {
            Style = (Style)Application.Current.Resources["GlobalContainer"],
            Padding = 20,
        };

        public FindNewJobPage() : base("Ramen Wage", false, false, true) {
            var grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            
            int rowCount = 0;
            for (int i = 0; i < App.JobCategories.Count; i++) {
                var topLeft = new Label {
                    Text = App.JobCategories[i].JobCategoryName,
                    Style = (Style)Application.Current.Resources["GlobalLabel"],
                };
                grid.Children.Add(topLeft, rowCount, 0);
                if ((i + 1) < App.JobCategories.Count) {
                    topLeft = new Label {
                        Text = App.JobCategories[i + 1].JobCategoryName,
                        Style = (Style)Application.Current.Resources["GlobalLabel"],
                    };
                    grid.Children.Add(topLeft, rowCount, 1);
                }
                rowCount++;
            }


            container.Children.Add(grid);

            MasterLayout.Children.Add(container);
            Content = MasterLayout;
        }
    }
}
