using FartCloud.Mobile.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FartCloud.Mobile.Pages {
    public class ForgotPasswordPage : BasePage {

        public ForgotPasswordPage() : base("Ramen Wage", false, true, false) {
            ExtStackLayout container = new ExtStackLayout() {
                Style = (Style)Application.Current.Resources["GlobalContainer"],
                Padding = 20,
            };

            Label forgotPasswordLabel = new Label() {
                Style = (Style)Application.Current.Resources["GlobalLabel"],
                Text = "Enter your email address and we will send you a forgot password link.",
            };

            Entry forgotPasswordEntry = new Entry() {
                Style = (Style)Application.Current.Resources["GlobalEntry"],
                Placeholder = "Email Address",
            };

            ExtButton sendPasswordResetButton = new ExtButton() {
                Style = (Style)Application.Current.Resources["GlobalButton"],
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Text = "Send Password Reset Email",
            };
            sendPasswordResetButton.Clicked += SendPasswordResetButton_Clicked;
            


            container.Children.Add(forgotPasswordLabel);
            container.Children.Add(forgotPasswordEntry);
            container.Children.Add(sendPasswordResetButton);

            MasterLayout.Children.Add(container);
            Content = MasterLayout;
        }

        private void SendPasswordResetButton_Clicked(object sender, EventArgs e) {
            ///Send password reset email
            Navigation.PopAsync();
        }
    }
}
