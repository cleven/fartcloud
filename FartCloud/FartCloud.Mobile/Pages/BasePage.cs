using FartCloud.Mobile.Controls;
using Xamarin.Forms;

namespace FartCloud.Mobile.Pages {
    public class BasePage : ContentPage {

        public BasePage() {

        }

        public TitleLayout NavigationBarView;
        public ExtStackLayout MasterLayout { get; protected set; }
       
        public BasePage(string _Title = "", bool _IsModal = false, bool _HasBackButton = true, bool _ShowLogo = false) {
            NavigationPage.SetHasNavigationBar(this, false);

            BackgroundColor = Color.White;

            MasterLayout = new ExtStackLayout {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Spacing = 0,
                Margin = 0,
            };

            NavigationBarView = new TitleLayout(_Title, _HasBackButton, _IsModal, _ShowLogo);
            MasterLayout.Children.Add(NavigationBarView);
        }
    }
}

