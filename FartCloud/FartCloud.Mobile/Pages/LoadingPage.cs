using System;
using Xamarin.Forms;

namespace FartCloud.Mobile.Pages {
    public class LoadingPage : BasePage {

        internal ActivityIndicator indicator = new ActivityIndicator() {
            IsRunning = true,
            HeightRequest = 50,
            WidthRequest = 50,
            VerticalOptions = LayoutOptions.CenterAndExpand,
            HorizontalOptions = LayoutOptions.Center
        };

        public LoadingPage() {
            NavigationPage.SetHasNavigationBar(this, false);

            Content = new StackLayout() {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Children = { indicator }
            };
        }
    }
}

