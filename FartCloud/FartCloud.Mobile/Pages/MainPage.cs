using FartCloud.Mobile.Controls;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FartCloud.Mobile.Pages {
    public class MainPage : BasePage {

        ExtButton acceptNewJobsButton = new ExtButton() {
            Style = (Style)Application.Current.Resources["GlobalButton"],
            Text = "ACCEPT NEW JOBS"
        };

        ExtButton viewAcceptedJobsButton = new ExtButton() {
            Style = (Style)Application.Current.Resources["GlobalButton"],
            Text = "VIEW ACCEPTED JOBS"
        };

        ExtButton completeCurrentJobsButton = new ExtButton() {
            Style = (Style)Application.Current.Resources["GlobalButton"],
            Text = "COMPLETE CURRENT JOB"
        };

        ExtStackLayout container = new ExtStackLayout() {
            Style = (Style)Application.Current.Resources["GlobalContainer"],
            Padding = 20,
        };

        public MainPage() : base("Ramen Wage", false, false, true) {
            acceptNewJobsButton.Clicked += AcceptNewJobsButton_Clicked;
            viewAcceptedJobsButton.Clicked += ViewAcceptedJobsButton_Clicked;
            completeCurrentJobsButton.Clicked += CompleteCurrentJobsButton_Clicked;

            container.Children.Add(acceptNewJobsButton);
            container.Children.Add(viewAcceptedJobsButton);
            container.Children.Add(completeCurrentJobsButton);

            
            MasterLayout.Children.Add(container);
            Content = MasterLayout;
        }

        private async void CompleteCurrentJobsButton_Clicked(object sender, System.EventArgs e) {
            await Navigation.PushAsync(new CurrentJobsPage());
        }

        private async void ViewAcceptedJobsButton_Clicked(object sender, System.EventArgs e) {
            await Navigation.PushAsync(new AcceptedJobsPage());
        }

        private async void AcceptNewJobsButton_Clicked(object sender, System.EventArgs e) {
            await Navigation.PushAsync(new FindNewJobPage());
        }
    }
}
