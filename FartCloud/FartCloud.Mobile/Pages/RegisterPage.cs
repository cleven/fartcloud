using FartCloud.Client;
using FartCloud.Mobile.Controls;
using FartCloud.Models.Data;
using FartCloud.Models.Interfaces;
using FartCloud.Models.MVVM_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace FartCloud.Mobile.Pages {
    public class RegisterPage : BasePage {

        ScrollView scrollView = new ScrollView() {
            HorizontalOptions = LayoutOptions.FillAndExpand,
            VerticalOptions = LayoutOptions.FillAndExpand,
        };

        ExtStackLayout container = new ExtStackLayout() {
            Style = (Style)Application.Current.Resources["GlobalContainer"],
            Padding = 20,
        };

        Entry firstNameEntry = new Entry() {
            Style = (Style)Application.Current.Resources["GlobalEntry"],
            Placeholder = "First Name",
        };
        Entry lastNameEntry = new Entry() {
            Style = (Style)Application.Current.Resources["GlobalEntry"],
            Placeholder = "Last Name",
        };
        Entry emailEntry = new Entry() {
            Style = (Style)Application.Current.Resources["GlobalEntry"],
            Placeholder = "Email Address",
        };
        Entry passwordEntry = new Entry() {
            Style = (Style)Application.Current.Resources["GlobalEntry"],
            Placeholder = "Password",
            IsPassword = true,
        };
        Entry phoneNumberEntry = new Entry() {
            Style = (Style)Application.Current.Resources["GlobalEntry"],
            Placeholder = "Phone",
        };
        ExtPicker jobCategoriesPicker = new ExtPicker() {
            Style = (Style)Application.Current.Resources["GlobalPicker"],
            Title = "Favorite Job Categories",
        };
        ExtPicker yearOfGraduationPicker = new ExtPicker() {
            Style = (Style)Application.Current.Resources["GlobalPicker"],
            Title = "Graduation Year",
        };
        ExtButton registerButton = new ExtButton() {
            Style = (Style)Application.Current.Resources["GlobalButton"],
            Text = "Sign Up!"
        };
        ListView jobCategoriesList = new ListView {
            ItemTemplate = new DataTemplate(typeof(CheckedListCell)),
            RowHeight = 40,
            Header = null,
            Footer = null,
        };
        Label registerLabel = new Label() {
            TextColor = Color.Red,
            IsVisible = false,
            HorizontalOptions = LayoutOptions.CenterAndExpand,
        };
        List<CheckedListCellViewModel> jobCategories = new List<CheckedListCellViewModel>();

        public List<CheckedListCellViewModel> GetJobCategoriesList(string filter = null) {
            if (!string.IsNullOrEmpty(filter)) {
                jobCategories = jobCategories
                    .Where(x => x.Text.StartsWith(filter, StringComparison.CurrentCultureIgnoreCase))
                    .ToList();
            }
            return jobCategories
                .OrderByDescending(x => x.IsSelected)
                .OrderBy(x => x.Text).ToList();
        }


        public RegisterPage() : base ("Ramen Wage", false, true, false) {
#if DEBUG
            firstNameEntry.Text = "Steven";
            lastNameEntry.Text = "Combs";
            emailEntry.Text = "combsmsteven@gmail.com";
            passwordEntry.Text = "b18b1skyt3";
            phoneNumberEntry.Text = "404-259-1118";
#endif

            foreach (JobCategory item in App.JobCategories) {
                jobCategories.Add(new CheckedListCellViewModel() {
                    Id = item.JobCategoryId,
                    Text = item.JobCategoryName,
                    IsSelected = false
                });
            }

            jobCategoriesList.ItemsSource = GetJobCategoriesList();
            jobCategoriesList.ItemTapped += JobCategoriesList_ItemTapped;
            registerButton.Clicked += RegisterButton_Clicked;

            int currentYear = DateTime.Now.Year;
            List<int> years = new List<int>();
            years.Add(currentYear);
            for (int i = 1; i < 4; i++) {
                years.Add(currentYear + i);
            }
            
            foreach (int year in years) {
                yearOfGraduationPicker.Items.Add(year.ToString());
            }
            

            container.Children.Add(firstNameEntry);
            container.Children.Add(lastNameEntry);
            container.Children.Add(emailEntry);
            container.Children.Add(passwordEntry);
            container.Children.Add(phoneNumberEntry);
            container.Children.Add(yearOfGraduationPicker);
            container.Children.Add(jobCategoriesList);
            container.Children.Add(registerLabel);
            container.Children.Add(registerButton);

            scrollView.Content = container;
            
            MasterLayout.Children.Add(scrollView);
            Content = MasterLayout;
        }

        private void JobCategoriesList_ItemTapped(object sender, ItemTappedEventArgs e) {
            var item = e.Item as CheckedListCellViewModel;
            JobCategoriesListItemTapped(item);
            var list = sender as ListView;
            list.ItemsSource = jobCategories;
        }

        public void JobCategoriesListItemTapped(CheckedListCellViewModel tapped) {
            if (tapped == null) return;
            foreach (var item in jobCategories) {
                if (item.Id == tapped.Id) {
                    item.IsSelected = !item.IsSelected;
                }
            }
        }

        private async void RegisterButton_Clicked(object sender, EventArgs e) {
            User model = new User() {
                EmailAddress = emailEntry.Text,
                FirstName = firstNameEntry.Text,
                LastName = lastNameEntry.Text,
                PasswordHash = passwordEntry.Text,
                Phone = phoneNumberEntry.Text,
            };
            
            var result = await UserClient.Register(model, jobCategories.Where(x=> x.IsSelected).Select(x=> x.Id).ToList());
            if (result.Errored) {
                registerLabel.IsVisible = true;
                registerLabel.Text = result.Exception;
                return;
            }

            CredentialHelper.Credentials.SaveCredentials(model.EmailAddress, result.Payload.Token, result.Payload.RefreshToken.ToString());
            await Navigation.PushAsync(new MainPage());
        }
    }
}
