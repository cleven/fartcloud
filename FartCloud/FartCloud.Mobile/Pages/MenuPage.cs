using FartCloud.Mobile.Config;
using FartCloud.Mobile.Controls;
using Xamarin.Forms;

namespace FartCloud.Mobile.Pages {
    public class MenuPage : ContentPage {
        public ListView Menu { get; set; }

        public MenuPage() {
            Title = "Menu";
            BackgroundColor = UIConstants.MenuBackgroundColor;
            Menu = new MenuListView();

            var menuLabel = new ContentView {
                Padding = new Thickness(10, 36, 0, 20),
                Content =
                    new Label {
                        FontSize = 28,
                        TextColor = Color.White,
                        Text = "MENU",
                    }
            };

            var layout = new StackLayout {
                Spacing = 0,
                VerticalOptions = LayoutOptions.FillAndExpand
            };


            layout.Children.Add(menuLabel);
            layout.Children.Add(Menu);

            var versionLabel = new ContentView {
                Content =
                    new Label {
                        FontSize = 14,
                        TextColor = Color.White,
                        Text = "Version: " + App.AppVersion,
                    }
            };
            layout.Children.Add(versionLabel);

            Content = layout;
        }
    }
}
