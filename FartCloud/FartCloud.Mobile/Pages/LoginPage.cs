using FartCloud.Client;
using FartCloud.Mobile.Controls;
using FartCloud.Models.Business;
using FartCloud.Models.Data;
using FartCloud.Models.Interfaces;
using System;
using Xamarin.Forms;

namespace FartCloud.Mobile.Pages {
    public class LoginPage : BasePage {

        ExtStackLayout container = new ExtStackLayout() {
            VerticalOptions = LayoutOptions.FillAndExpand,
            HorizontalOptions = LayoutOptions.FillAndExpand,
        };

        Entry usernameEntry = new Entry() {
            Style = (Style)Application.Current.Resources["GlobalEntry"],
            Placeholder = "Username",
            TextColor = Color.White,
        };

        Entry passwordEntry = new Entry() {
            Style = (Style)Application.Current.Resources["GlobalEntry"],
            Placeholder = "Password",
            IsPassword = true,
            TextColor = Color.White,
        };

        ExtButton loginButton = new ExtButton() {
            Style = (Style)Application.Current.Resources["GlobalButton"],
            Text = "Login",
        };
        ExtButton forgotPassword = new ExtButton() {
            Style = (Style)Application.Current.Resources["GlobalButton"],
            Text = "Forgot Password?",
            TextColor = Color.White,
            BackgroundColor = Color.Transparent,
            HorizontalOptions = LayoutOptions.FillAndExpand,
            WidthRequest = 130,
        };
        Button signUpButton = new Button() {
            Style = (Style)Application.Current.Resources["GlobalButton"],
            Text = "Sign Up",
            HorizontalOptions = LayoutOptions.FillAndExpand,
            WidthRequest = 130,
        };
        ExtStackLayout loginContainer = new ExtStackLayout() {
            VerticalOptions = LayoutOptions.CenterAndExpand,
            HorizontalOptions = LayoutOptions.FillAndExpand,
            Padding = 20
        };

        ExtStackLayout signUpContainer = new ExtStackLayout() {
            HorizontalOptions = LayoutOptions.FillAndExpand,
            VerticalOptions = LayoutOptions.End,
            Orientation = StackOrientation.Horizontal,
            Spacing = 1,
        };
        Label loginLabel = new Label() {
            TextColor = Color.Red,
            IsVisible = false,
            HorizontalOptions = LayoutOptions.CenterAndExpand,
        };

        public LoginPage() : base("Ramen Wage", false, false, false) {
#if DEBUG
            usernameEntry.Text = "combsmsteven@gmail.com";
            passwordEntry.Text = "b18b1skyt3";
#endif

            forgotPassword.Clicked += ForgotPassword_Clicked;
            signUpButton.Clicked += SignUpButton_Clicked;
            loginButton.Clicked += LoginButton_Clicked;
            
            loginContainer.Children.Add(usernameEntry);
            loginContainer.Children.Add(passwordEntry);
            loginContainer.Children.Add(loginLabel);
            loginContainer.Children.Add(loginButton);
            loginContainer.Children.Add(forgotPassword);
            
            //signUpContainer.Children.Add(forgotPassword);
            signUpContainer.Children.Add(signUpButton);
            
            container.Children.Add(loginContainer);
            container.Children.Add(signUpContainer);

            MasterLayout.Children.Add(container);
            Content = MasterLayout;
        }

        private async void LoginButton_Clicked(object sender, EventArgs e) {
            loginLabel.IsVisible = false;
            LoginModel model = new LoginModel() {
                Username = usernameEntry.Text,
                Password = passwordEntry.Text,
            };

            var result = await UserClient.Login(model);
            if (result.Errored) {
                loginLabel.IsVisible = true;
                loginLabel.Text = result.Exception;
                return;
            }

            CredentialHelper.Credentials.SaveCredentials(model.Username, result.Payload.Token, result.Payload.RefreshToken.ToString());
            await Navigation.PushAsync(new MainPage());
        }

        private async void ForgotPassword_Clicked(object sender, EventArgs e) {
            await Navigation.PushAsync(new ForgotPasswordPage());
        }

        private async void SignUpButton_Clicked(object sender, EventArgs e) {
            await Navigation.PushAsync(new RegisterPage());
        }

        protected override void OnAppearing() {
            base.OnAppearing();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
