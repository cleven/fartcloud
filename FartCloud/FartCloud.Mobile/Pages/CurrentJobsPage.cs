using FartCloud.Mobile.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FartCloud.Mobile.Pages {
    public class CurrentJobsPage : BasePage {

        ExtStackLayout container = new ExtStackLayout() {
            Style = (Style)Application.Current.Resources["GlobalContainer"],
            Padding = 20,
        };

        public CurrentJobsPage() : base("Ramen Wage", false, false, true) {

            MasterLayout.Children.Add(container);
            Content = MasterLayout;
        }
    }
}
