using FartCloud.Mobile.Controls;
using FartCloud.Mobile.Models;
using FartCloud.Models.Interfaces;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace FartCloud.Mobile.Pages {

    public interface IRootPage {
        void ResetPage(int index);
        void Present();
    }

    public class RootPageHelper {
        public static IRootPage RootPage { get; set; }
    }

    public class RootPage : MasterDetailPage, IRootPage {

        MenuPage menuPage = new MenuPage();
        bool hasBackButton;

        public RootPage() {
            RootPageHelper.RootPage = this;
            Master = menuPage;

            if (!App.CredentialService.DoCredentialsExist()) {
                Detail = new NavigationPage(new LoginPage()) {
                    BarTextColor = Color.FromHex("ffffff"),
                    BarBackgroundColor = Color.FromHex("72279b"),
                };
            } else {
                Detail = new NavigationPage(new MainPage()) {
                    BarTextColor = Color.FromHex("ffffff"),
                    BarBackgroundColor = Color.FromHex("72279b")
                };
            }

            menuPage.Menu.ItemTapped += (sender, e) => NavigateTo(e.Item as MenuModel);
            IsPresentedChanged += RootPage_IsPresentedChanged;
        }

        private void RootPage_IsPresentedChanged(object sender, EventArgs e) {
            var master = sender as MasterDetailPage;
            var detail = master.Detail as NavigationPage;
            var basePage = detail.CurrentPage as BasePage;
            if (!IsPresented) {
                if (hasBackButton)
                    basePage.NavigationBarView.BackButton.IsVisible = true;
                else
                    basePage.NavigationBarView.BackButton.IsVisible = false;
            } else {
                if (basePage.NavigationBarView.BackButton.IsVisible)
                    hasBackButton = true;
                basePage.NavigationBarView.BackButton.IsVisible = false;
            }
        }

        public void Present() {
            menuPage.Menu.SelectedItem = null;
            IsPresented = !IsPresented;
        }

        public void NavigateTo(MenuModel item) {
            menuPage.Menu.SelectedItem = null;
            if (item.TargetType != null) {
                if (item.TargetType == typeof(MainPage)) {
                    IsPresented = false;
                    Detail = new NavigationPage(new MainPage()) {
                        BarTextColor = Color.FromHex("FFFFFF"),
                        BarBackgroundColor = Color.FromHex("72279b")
                    };
                    return;
                }
                
            } else {
                if (item.Title == "Rate Us") {
                    string appleAppLink = string.Empty;//App.Configuration.GetValue("AppleAppLinkUri");
                    string appleHttpLink = string.Empty;//App.Configuration.GetValue("AppleHttpLinkUri");
                    string appleId = string.Empty;//App.Configuration.GetValue("AppleId");
                    string googleAppLink = string.Empty;//App.Configuration.GetValue("GoogleAppLinkUri");
                    string googleHttpLink = string.Empty;//App.Configuration.GetValue("GoogleHttpLinkUri");
                    switch (Device.RuntimePlatform) {
                        case "iOS":
                            FartCloud.Mobile.Interfaces.AppLinkService.AppLink.OpenStoreLink(appleAppLink, appleHttpLink, appleId);
                            break;
                        case "Android":
                            FartCloud.Mobile.Interfaces.AppLinkService.AppLink.OpenStoreLink(googleAppLink, googleHttpLink, null);
                            break;
                    }
                    
                    ResetSelectedMenuItem(0);
                } else if (item.Title == "Help") {
                    Uri uri = new Uri("");
                    Device.OpenUri(uri);
                } else if (item.Title == "Call us") {
                    string phone = string.Empty;
                    string phoneUrl = string.Empty;
                    switch (Device.RuntimePlatform) {
                        case "iOS":
                            phoneUrl = "tel:" + phone;
                            break;
                        case "Android":
                            phoneUrl = "tel+" + phone;
                            break;
                    }
                    Uri uri = new Uri(phoneUrl);
                    Device.OpenUri(uri);
                } else if (item.Title == "Facebook") {
                    Uri uri = new Uri("");
                    Device.OpenUri(uri);
                } else if (item.Title == "Twitter") {
                    Uri uri = new Uri("");
                    Device.OpenUri(uri);
                } else if (item.Title == "LinkedIn") {
                    Uri uri = new Uri("");
                    Device.OpenUri(uri);
                } else if (item.Title == "Logout") {
                    AppHelper.App.Logout();
                }
            }
            IsPresented = false;
        }


        public void ResetPage(int index) {
            ResetSelectedMenuItem(index);
        }

        public void ResetSelectedMenuItem(int index) {
            List<MenuModel> data = new MenuListData();
            menuPage.Menu.ItemsSource = data;
            menuPage.Menu.SelectedItem = data[index];
        }
    }
}
