using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FartCloud.Mobile.Config {
    public class UIConstants {

        public static Color TitleBarColor { get; set; }
        public static Color TitleBarTextColor { get; set; }
        public static Color PrimaryColor { get; set; }
        public static Color MenuBackgroundColor { get; set; }
    }
}
