using FartCloud.Client;
using FartCloud.Mobile.Pages;
using FartCloud.Models.Data;
using FartCloud.Models.Business;
using FartCloud.Models.Interfaces;
using System.Collections.Generic;
using Xamarin.Forms;
using FartCloud.WebControl;
using FartCloud.Mobile.Interfaces;
using System;

namespace FartCloud.Mobile {
    
    

    public class App : Application, IApp {

        public static string AppVersion { get; set; }
        public static List<AppConfig> AppConfigs { get; set; }
        public static List<JobCategory> JobCategories { get; set; }
        public static string AppName = "FartCloud.;
        public static ICredentialsService CredentialService { get; set; }
        public static bool HighlightBackgrounds { get; set; }
        public static Random rand { get; set; } 

        public App() {
            WebClientConstants.Url = "http://dev-FartCloud.gsgus.com";
            LoggerClient.Log("App.cs is initialized");
            AppConfigs = new List<AppConfig>();
            JobCategories = new List<JobCategory>();

            HighlightBackgrounds = false;

            AppHelper.App = this;
            CredentialService = DependencyService.Get<ICredentialsService>();

            Resources = new ResourceDictionary();
            Resources.Add("GlobalButton", Styles.GlobalButton);
            Resources.Add("GlobalEntry", Styles.GlobalEntry);
            Resources.Add("GlobalLabel", Styles.GlobalLabel);
            Resources.Add("GlobalPicker", Styles.GlobalPicker);
            Resources.Add("GlobalContainer", Styles.GlobalContainer);
            

            SettingClient.GetAppSettings(AppHelper.App);
            LoggerClient.Log("Setting loading page");
            MainPage = new LoadingPage();
        }
        
        public void Logout() {
            LoggerClient.Log("User hit logout");
            CredentialService.DeleteCredentials();
            var navigationPage = new RootPage();
            MainPage = navigationPage;
        }

        public void SetMainPage(Settings settings) {
            Device.BeginInvokeOnMainThread(() => {
                AppConfigs = settings.AppConfigs;
                JobCategories = settings.JobCategories;

                LoggerClient.Log("Setting main page.");
                var navigationPage = new RootPage();
                MainPage = navigationPage;
            });
        }

        public static Color GetRandomColor() {
            if (App.rand == null)
                App.rand = new Random();

            int a = App.rand.Next(255);
            int r = App.rand.Next(255);
            int g = App.rand.Next(255);
            int b = App.rand.Next(255);
            Color color = Color.FromRgba(r, g, b, a);
            return color;
        }
        

        
    }
}
