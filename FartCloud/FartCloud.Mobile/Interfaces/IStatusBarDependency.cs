
namespace FartCloud.Mobile.Interfaces {
    public interface IStatusBarDependency {
        void Set(bool statusBarHidden);
    }
}
