
namespace FartCloud.Mobile.Interfaces {
    public interface IAppLinkService {
        /// <summary>
        /// Look in platform specific projects for implementation. 
        /// Droid : MainActivity.cs
        /// iOS : AppDelegate.cs
        /// </summary>
        /// <param name="storeLinkPath"></param>
        void OpenStoreLink(string storeLinkPath, string httpStoreLinkPath, string appleId);
    }

    public class AppLinkService {
        public static IAppLinkService AppLink { get; set; }
    }
}
