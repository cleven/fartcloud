using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FartCloud.Mobile.Controls {
    public class ExtStackLayout : StackLayout {

        public ExtStackLayout() {
            if (App.HighlightBackgrounds) {
                BackgroundColor = App.GetRandomColor();
            }
        }
    }
}
