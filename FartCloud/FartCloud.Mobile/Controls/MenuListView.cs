using Xamarin.Forms;
using System.Collections.Generic;
using FartCloud.Mobile.Models;

namespace FartCloud.Mobile.Controls {
    public class MenuListView : ListView {
        public MenuListView() {
            List<MenuModel> data = new MenuListData();
            Header = new Label { HeightRequest = 0 };
            Footer = new Label { HeightRequest = 0 };
            ItemsSource = data;
            VerticalOptions = LayoutOptions.FillAndExpand;
            BackgroundColor = Color.Transparent;

            switch (Device.RuntimePlatform) {
                case "iOS":
                    RowHeight = 40;
                    break;
                case "Android":
                    RowHeight = 45;
                    break;
            }

            var cell = new DataTemplate(typeof(MenuCell));

            SelectedItem = data[0];
            ItemTemplate = cell;
        }
    }



    public class MenuListData : List<FartCloud.Mobile.Models.MenuModel> {
        public MenuListData() {
            Add(new FartCloud.Mobile.Models.MenuModel() {
                Title = "Logout",
                IconSource = "ic_exit_to_app_black_24dp.png",
            });
            Add(new FartCloud.Mobile.Models.MenuModel() {
                Title = "Rate Us",
                IconSource = "ic_star_black_24dp.png",
            });
            Add(new FartCloud.Mobile.Models.MenuModel() {
                Title = "Help",
                IconSource = "ic_help_black_24dp.png",
            });
            Add(new FartCloud.Mobile.Models.MenuModel() {
                Title = "Facebook",
                IconSource = "facebook.png"
            });
            Add(new FartCloud.Mobile.Models.MenuModel() {
                Title = "Twitter",
                IconSource = "twitter.png"
            });
            Add(new FartCloud.Mobile.Models.MenuModel() {
                Title = "LinkedIn",
                IconSource = "linkedin.png"
            });
        }
    }
}

