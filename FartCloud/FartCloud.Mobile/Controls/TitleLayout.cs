using FartCloud.Mobile.Config;
using FartCloud.Mobile.Pages;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FartCloud.Mobile.Controls {

    public class TitleLayout : ExtStackLayout {

        private bool IsModal { get; set; }
        private bool ShowBackButton { get; set; }
        private bool ShowLogo { get; set; }

        public Func<Task<bool>> CustomBackButtonAction { get; set; }

        public string Title {
            get { return TitleLabel.Text; }
            set { TitleLabel.Text = value; }
        }
        
        internal TitleLabel TitleLabel = new TitleLabel {
            Style = (Style)Application.Current.Resources["GlobalLabel"],
            FontSize = 20,
            VerticalOptions = LayoutOptions.CenterAndExpand,
            MinimumWidthRequest = 125,
            TextColor = Color.White,
        };
        internal Image Logo = new Image() {
            Source = "logo.png",
            IsVisible = false,
            VerticalOptions = LayoutOptions.CenterAndExpand,
            HeightRequest = 25,
            WidthRequest = 25
        };
        internal ExtStackLayout TitleContainer = new ExtStackLayout {
            Orientation = StackOrientation.Horizontal,
            HorizontalOptions = LayoutOptions.CenterAndExpand,
            VerticalOptions = LayoutOptions.FillAndExpand,
            MinimumWidthRequest = 150,
            HeightRequest = 25,
        };
        

        #region Action Left Container
        public Image CloseButton = new Image() {
            VerticalOptions = LayoutOptions.CenterAndExpand,
            IsVisible = false,
        };
        public Image BackButton = new Image() {
            VerticalOptions = LayoutOptions.CenterAndExpand,
            IsVisible = false,
        };
        
        internal ExtStackLayout ActionLeftContainer = new ExtStackLayout() {
            HorizontalOptions = LayoutOptions.Start,
            VerticalOptions = LayoutOptions.FillAndExpand,
            WidthRequest = 30,
            Padding = 0,
            Margin = 0,
            Spacing = 0
        };
        #endregion

        #region Menu Container
        public Image MenuButton = new Image() {
            VerticalOptions = LayoutOptions.CenterAndExpand,
            WidthRequest = 30,
        };
        internal ExtStackLayout MenuContainer = new ExtStackLayout() {
            HorizontalOptions = LayoutOptions.End,
            VerticalOptions = LayoutOptions.FillAndExpand,
            WidthRequest = 30,
            Padding = 0,
            Margin = 0,
            Spacing = 0
        };
        #endregion

        public TitleLayout(string _Title, bool _ShowBackButton = false, bool _IsModal = false, bool _ShowLogo = true) {
            IsModal = _IsModal;
            ShowBackButton = _ShowBackButton;
            ShowLogo = _ShowLogo;
            Title = _Title;

            TitleContainer.Children.Add(TitleLabel);
            TitleContainer.Children.Add(Logo);
            ActionLeftContainer.Children.Add(CloseButton);
            ActionLeftContainer.Children.Add(BackButton);
            MenuContainer.Children.Add(MenuButton);

            this.BackgroundColor = Color.White;
            switch (Device.RuntimePlatform) {
                case "Android":
                    HeightRequest = 40;
                    MenuButton.Source = ImageSource.FromFile("ic_menu_black_24dp.png") as FileImageSource;
                    BackButton.Source = ImageSource.FromFile("ic_arrow_back_black_24dp.png") as FileImageSource;
                    BackButton.WidthRequest = 30;
                    CloseButton.Source = ImageSource.FromFile("ic_close_black_24dp.png") as FileImageSource;
                    CloseButton.WidthRequest = 30;
                    break;
                case "iOS":
                    HeightRequest = 35;
                    MenuButton.Source = ImageSource.FromFile("Images/Menu/menu.png") as FileImageSource;
                    BackButton.Source = ImageSource.FromFile("Images/Back/back.png") as FileImageSource;
                    BackButton.WidthRequest = 25;
                    CloseButton.Source = ImageSource.FromFile("Images/Cancel/cancel.png") as FileImageSource;
                    CloseButton.WidthRequest = 25;
                    break;
            }
            
            this.Padding = new Thickness(5, 5, 5, 5);
            this.Spacing = 0;
            this.Orientation = StackOrientation.Horizontal;
            this.Children.Add(ActionLeftContainer);
            this.Children.Add(TitleContainer);
            this.Children.Add(MenuContainer);
            

            if (IsModal) {
                MenuButton.IsVisible = false;
                BackButton.IsVisible = false;
                CloseButton.IsVisible = true;
            }

            if (ShowBackButton) {
                CloseButton.IsVisible = false;
                BackButton.IsVisible = true;
                Logo.IsVisible = false;
            }

            if (ShowLogo) {
                Logo.IsVisible = true;
            }


            var ActionLeftContainerTap = new TapGestureRecognizer();
            ActionLeftContainerTap.Tapped += ActionLeftContainerTap_Clicked;
            ActionLeftContainer.GestureRecognizers.Add(ActionLeftContainerTap);

            var MenuButtonTap = new TapGestureRecognizer();
            MenuButtonTap.Tapped += MenuButton_Clicked;
            MenuContainer.GestureRecognizers.Add(MenuButtonTap);
        }

        void MenuButton_Clicked(object sender, EventArgs e) {
            if (RootPageHelper.RootPage == null)
                return;
            RootPageHelper.RootPage.Present();
        }

        async void ActionLeftContainerTap_Clicked(object sender, EventArgs e) {
            if (CustomBackButtonAction != null) {
                var cancelNavigation = await CustomBackButtonAction.Invoke();
                if (!cancelNavigation) {
                    NavigateBack();
                }
            } else {
                NavigateBack();
            }
        }

        private void NavigateBack() {
            if (IsModal) {
                Navigation.PopModalAsync();
            } else {
                Navigation.PopAsync();
            }
        }
    }
}
