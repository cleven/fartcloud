using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FartCloud.Mobile.Controls {
    public class ExtPicker : Picker {

        public static readonly BindableProperty HintColorProperty =
            BindableProperty.Create("HintColor", typeof(Color), typeof(ExtPicker), Color.Default);

        public Color HintColor {
            get { return (Color)GetValue(HintColorProperty); }
            set { SetValue(HintColorProperty, value); }
        }
    }
}
