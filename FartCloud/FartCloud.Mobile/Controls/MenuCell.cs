using Xamarin.Forms;
namespace FartCloud.Mobile.Controls {
    public class MenuCell : ViewCell {
        public MenuCell() {

            var layout = CreateNameLayout();
            View = layout;
        }

        public StackLayout CreateNameLayout() {

            var image = new Image {
                HorizontalOptions = LayoutOptions.Start
            };
            image.SetBinding(Image.SourceProperty, new Binding("IconSource"));
            var label = new Label {
                FontSize = 20,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                TextColor = Color.White
            };
            label.SetBinding(Label.TextProperty, "Title");


            StackLayout layout = new StackLayout {
                Padding = 10,
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Children = { image, label }
            };

            return layout;
        }
    }
}

