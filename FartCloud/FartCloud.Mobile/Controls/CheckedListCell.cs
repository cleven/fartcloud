using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FartCloud.Mobile.Controls {
    
    public class CheckedListCell : ViewCell {
        Image image = new Image {
            HorizontalOptions = LayoutOptions.End,
            HeightRequest = 15,
            WidthRequest = 15
        };
        StackLayout imageContainer = new StackLayout {
            Padding = new Thickness(0, 0, 15, 0)
        };
        RelativeLayout layout = new RelativeLayout {
            Padding = new Thickness(5),
            VerticalOptions = LayoutOptions.FillAndExpand,
            HorizontalOptions = LayoutOptions.FillAndExpand,
            BackgroundColor = Color.FromHex("fbfbfb")
        };
        Label label = new Label {
            Text = "Hello",
            FontSize = 14,
            HorizontalOptions = LayoutOptions.StartAndExpand,
            VerticalOptions = LayoutOptions.Center,
            TextColor = Color.Black
        };

        public CheckedListCell() {
            LayoutComponents();
            SetBindings();
            SetContent();
        }
        

        void LayoutComponents() {
            imageContainer.Children.Add(image);

            layout.Children.Add(label,
                Constraint.Constant(5),
                Constraint.RelativeToParent((p) => (p.Height / 2) - 10),
                Constraint.RelativeToParent((p) => (p.Width * .9) - 5)
            );
            layout.Children.Add(imageContainer,
                Constraint.RelativeToView(label, (p, s) => {
                    return s.X + s.Width;
                }),
                Constraint.RelativeToParent((p) => (p.Height / 2) - 10),
                Constraint.RelativeToParent((p) => p.Width * .1)
            );
        }

        void SetBindings() {
            image.SetBinding(Image.SourceProperty, "ImageSource");
            label.SetBinding(Label.TextProperty, "Text");
        }

        void SetContent() {
            View = layout;
        }
    }

}
