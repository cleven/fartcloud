﻿Write-Host "There are 3 steps to this script listed below"
Write-Host "1.) Refactor script - Refactors all project files / directories / namespaces"
Write-Host "2.) Source control script - Adds your solution to Source Control i.e. BitBucket"
Write-Host "3.) Config copying script - Copies any *.template.config to *.config useful for not having to copy them yourself"

$ProceedRefactor = Read-Host "Refactor script - Would you like to refactor all project files / directories / namespaces?(y,n)"
if ($ProceedRefactor.ToLower() -eq "y") {
    $CurrentProjectName = Read-Host -Prompt "Please enter current project name"
    $ProjectName = Read-Host -Prompt "Please enter a new project name"

    [string[]]$Paths = @('.\')
    [string[]]$DirIncludes = @("*$CurrentProjectName*")
    [string[]]$DirExcludes = @('*bin*', '*obj*', '*packages*', '*Scripts*', '*Content*', '*.nuget*', '*fonts*')
    $dirs = Get-ChildItem $Paths -Directory -Recurse -Exclude $DirExcludes -Include $DirIncludes | %{$_.FullName} | Sort-Object -Property Length -Descending | 
    %{ 
        $Item = Get-Item $_
        $allowed = $true
        foreach ($exclude in $Excludes) { 
            if ((Split-Path $Item.FullName -Parent) -ilike $exclude) { 
                $allowed = $false
                break
            }
        }
        if ($allowed) {
            $Item
        }
    }

    Write-Host ""
    Write-Host "Renaming all following directories"
    foreach ($f in $dirs) {
        if ($f.FullName.Contains("obj")){
            continue;
        }

        Write-Host $f.Name "->" $f.Name.Replace("$CurrentProjectName", "$ProjectName")
        Rename-Item $f.FullName $f.Name.Replace("$CurrentProjectName", "$ProjectName")
    }

    
    
    [string[]]$Paths = @('.\')
    [string[]]$FileIncludes = @('*.cs', '*.sqlproj', '*.csproj*', '*.cshtml', '*.sln', '*.asax', '*.config', '*.xaml', '*.xml')
    [string[]]$FileExcludes = @('*bin*', '*obj*', '*packages*', '*Scripts*', '*Content*', '*.nuget*', '*fonts*')

    $files = Get-ChildItem $Paths -File -Recurse -Exclude $FileExcludes -Include $FileIncludes | %{$_.FullName} | Sort-Object -Property Length -Descending | 
    %{ 
        $Item = Get-Item $_
        $allowed = $false

        $isInExcludes = $false
        foreach ($exclude in $FileExcludes) { 
            if ($Item.FullName -ilike $exclude) { 
                $isInExcludes = $true
                break
            }
        }
        if ($isInExcludes -eq $true) {
            return
        }


        $lines = get-content $Item.FullName
        $hasReplaceableContent = $false
        foreach ($line in $lines) {
            if ($line -like "*$CurrentProjectName*") {
                $hasReplaceableContent = $true
                break;
            }
        }
        if ($hasReplaceableContent -eq $true) {
            $allowed = $true
        }

        if ($Item.Name -like "*$CurrentProjectName*") {
           $allowed = $true
        }

        if ($allowed) {
            $Item
        }
    }

    Write-Host ""
    Write-Host "Renaming all following files"
    foreach ($f in $files) {
        if ($f.FullName.Contains("obj")){
            continue;
        }

        Write-Host $f.FullName "->" $f.Name.Replace("$CurrentProjectName", "$ProjectName")
        (get-content $f) | foreach-object { $_ -replace "$CurrentProjectName\\", "$ProjectName\" } | set-content $f
        (get-content $f) | foreach-object { $_ -replace "$CurrentProjectName.", "$ProjectName." } | set-content $f
        Rename-Item $f.FullName $f.Name.Replace("$CurrentProjectName", "$ProjectName")
    }
} else {
    Write-Host "No action taken"
}
Write-Host ""

$ProceedSourceControl = Read-Host "2.) Source control script - Would you like to add this project to source control?(y,n)"
if ($ProceedSourceControl.ToLower() -eq "y") {
	$RepoName = Read-Host "Enter in a new repository name for your solution ($ProjectName)"
    if ($RepoName -eq "") {
        $RepoName = $ProjectName
    }
	$GitUsername = Read-Host "Please enter your BitBucket username"
	$TempGitPassword = Read-Host -AsSecureString "Please enter your BitBucket password"
	$GitPassword = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($TempGitPassword))
	$GitTeamName = Read-Host "Please enter your BitBucket team"
	Write-Host "Removing initial git repository information"
	Remove-Item -Recurse -Force ".git"

	Write-Host "Initializing new git repo"
	Start-Process -FilePath "C:\Program Files\Git\bin\git.exe" -ArgumentList "init" -Wait
    Write-Host "Adding all files to repo"
	Start-Process -FilePath "C:\Program Files\Git\bin\git.exe" -ArgumentList "add ." -Wait
    Write-Host "Commiting all files to new repository"
	Start-Process -FilePath "C:\Program Files\Git\bin\bash.exe" -ArgumentList "--login -c 'git commit -am 'auto : initial''" -Wait
    Write-Host "Creating new repository in bitbucket"
	Start-Process -FilePath "C:\Program Files\Git\bin\bash.exe" -ArgumentList "--login -c 'curl --user ${GitUsername}:$GitPassword https://api.bitbucket.org/1.0/repositories/ --data name=$RepoName --data owner=$GitTeamName'" -Wait
    Write-Host "Setting remote url for origin in current repo"
	Start-Process -FilePath "C:\Program Files\Git\bin\bash.exe" -ArgumentList "--login -c 'git remote add origin git@bitbucket.org:$GitTeamName/$RepoName.git'" -Wait
    Write-Host "Pushing changes to master branch"
	Start-Process -FilePath "C:\Program Files\Git\bin\bash.exe" -ArgumentList "--login -c 'git push --set-upstream origin master'" -Wait
    Write-Host "Creating development branch"
	Start-Process -FilePath "C:\Program Files\Git\bin\bash.exe" -ArgumentList "--login -c 'git checkout -b development'" -Wait
    Write-Host "Adding initial.txt file to development branch"
	Start-Process -FilePath "C:\Program Files\Git\bin\bash.exe" -ArgumentList "--login -c 'echo 'initial' >> initial.txt && git add . && git commit -am 'auto : initial''" -Wait
    Write-Host "Pushing changes to development"
	Start-Process -FilePath "C:\Program Files\Git\bin\bash.exe" -ArgumentList "--login -c 'git push --set-upstream origin development'" -Wait
} else {
    $ProceedRemoveInitialSourceControl = Read-Host "2.1.) Config copying script - Would you like to remove initial source control bindings?(y,n)"
    if ($ProceedRemoveInitialSourceControl.ToLower() -eq "y") {
	    Write-Host "Removing initial git repository information"
	    Start-Process -FilePath "C:\Program Files\Git\bin\bash.exe" -ArgumentList "rm -rf .git" -Wait
    } else {
        Write-Host "No action taken"
    }
}
Write-Host ""

$ProceedResetConfig = Read-Host "3.) Config copying script - Would you like to copy all *.template.config's to *.config?(y,n)"
if ($ProceedResetConfig.ToLower() -eq "y") {
    Invoke-Expression ".\ResetConfigsFromTemplate.ps1"
} else {
    Write-Host "No action taken"
}
Write-Host ""

Read-Host "All done!"