# !/bin/sh

function fail {
    echo "$*" >&2
    exit 1
}

function section_print {
    echo "\n=== $* ==="
}


INFO_PLIST="$PROJECT_FOLDER/Info.plist"
BUILD_NUMBER_FILE="$PROJECT_FOLDER/build_num.txt"

echo "Configuration : " $CONFIGURATION
echo "Device : " $DEVICE
echo "Workspace : " $WORKSPACE
echo "Project Folder : " $PROJECT_FOLDER
echo "Solution File : " $SOLUTION_FILE
echo "Build Path : " $BUILD_PATH
echo "Info.plist : " $INFO_PLIST
echo "Build Number File : " $BUILD_NUMBER_FILE

section_print "Updating Info.plist"
cd "$WORKSPACE"
if [ -z "$VERSION_NUMBER" ]; then 
  VERSION_NUMBER=$(/usr/libexec/PlistBuddy -c "Print CFBundleShortVersionString" "$INFO_PLIST")

  if [ ! -e $BUILD_NUMBER_FILE ]; then
    echo >> $BUILD_NUMBER_FILE
    echo "0" > $BUILD_NUMBER_FILE
  fi

  BUILD_NUMBER=$(cat $BUILD_NUMBER_FILE)
  BUILD_NUMBER=$(expr $BUILD_NUMBER + 1)
  echo "Setting Build Number : $BUILD_NUMBER"
  echo $BUILD_NUMBER > $BUILD_NUMBER_FILE
  VERSION=$VERSION_NUMBER.$BUILD_NUMBER

else

  echo "Version number is being populated externally"
  VERSION=$VERSION_NUMBER

fi

echo "Setting CFBundleVersion : $VERSION"
/usr/libexec/PlistBuddy -c "Set CFBundleVersion $VERSION" "$INFO_PLIST"

bundleDisplayName="TitleClose - Mobile - $VERSION"

echo "Setting CFBundleDisplayName : $bundleDisplayName"
/usr/libexec/PlistBuddy -c "Set CFBundleDisplayName $bundleDisplayName" "$INFO_PLIST"

section_print "Restoring nuget packages"
/usr/local/bin/nuget restore 

section_print "Building $CONFIGURATION"
/Applications/Xamarin\ Studio.app/Contents/MacOS/mdtool -v build "--configuration:$CONFIGURATION|$DEVICE" "$SOLUTION_FILE" || fail "Build failed"

# Get the .app and .ipa file names
section_print "Obtaining .app and .ipa files"
cd "$BUILD_PATH"
for file in "*.app"
do
    APP_NAME=`echo $file`
    echo $file
done
APP_NAME=${APP_NAME%.*}
IPA_FILE="$APP_NAME-$VERSION.ipa"
echo IPA_FILE

section_print "Compressing dSYM"
DSYM_FILE="$APP_NAME-$VERSION.dSYM.zip"
zip -r $DSYM_FILE "$APP_NAME.app.dSYM"

section_print "Removing old artefacts from Workspace folder"
cd "$WORKSPACE"
rm -f -v Ad-Hoc*.ipa
rm -f -v Ad-Hoc*.dSYM.zip
rm -rf -v Ad-Hoc*.dSYM

section_print "Copying artefacts from Build folder"

echo "Entering Directory : $BUILD_PATH"
cd "$BUILD_PATH"

if [ $CONFIGURATION = "Ad-Hoc" ]; then
  echo "Copying $IPA_FILE to $WORKSPACE/"
  cp -f -v "$IPA_FILE" "$WORKSPACE/$CONFIGURATION-$APP_NAME.ipa" || fail "Failed to copy ipa"

  echo "Copying $DSYM_FILE to $WORKSPACE/"
  cp -v "$DSYM_FILE" "$WORKSPACE/$CONFIGURATION-$DSYM_FILE" || fail "Failed to copy dSYM zip"

  echo "Copying $APP_NAME.app.dSYM to $WORKSPACE/$APP_NAME.app.dSYM/"
  cp -r "$APP_NAME.app.dSYM" "$WORKSPACE/$CONFIGURATION-$APP_NAME.app.dSYM/" || fail "Failed to copy dSYM"

else

  echo "Copying $IPA_FILE to $HOME/"
  cp -v "$IPA_FILE" "$HOME/$CONFIGURATION-$IPA_FILE" || fail "Failed to copy ipa"

  echo "Copying $DSYM_FILE to $HOME/"
  cp -v "$DSYM_FILE" "$HOME/$CONFIGURATION-$DSYM_FILE" || fail "Failed to copy dSYM zip"

  echo "Copying $APP_NAME.app.dSYM to $HOME/$APP_NAME.app.dSYM/"
  cp -r "$APP_NAME.app.dSYM" "$HOME/$CONFIGURATION-$APP_NAME.app.dSYM/" || fail "Failed to copy dSYM"
fi


section_print "Build succeeded"
